<?php

?>
<style>
      .controls {
        margin-top: 10px;
        border: 1px solid transparent;
        border-radius: 2px 0 0 2px;
        box-sizing: border-box;
        -moz-box-sizing: border-box;
        height: 32px;
        outline: none;
        box-shadow: 0 2px 6px rgba(0, 0, 0, 0.3);
      }

      #pac-input {
        background-color: #fff;
        font-family: Roboto;
        font-size: 15px;
        font-weight: 300;
        margin-left: 12px;
        padding: 0 11px 0 13px;
        text-overflow: ellipsis;
        width: 300px;
      }

      #pac-input:focus {
        border-color: #4d90fe;
      }
    .info-box-currency {
      overflow: hidden;
      height: 276px;
      background: #fcf3c0;
    }
    .info-box-currency h5 {
        font-weight: 300;
        line-height: 50px;
        height: 50px;
        margin: 0;
        padding: 0 15px;
        background: #fbe3a9;
    }
    .info-box-currency .table-holder {
        font-size: 18px;
        padding: 15px;
    }
    .info-box-currency .table-holder .table > thead {
        font-size: 14px;
        font-weight: 300;
        font-style: italic;
    }
    h5, .h5 {
        font-size: 18px;
    }
    </style>

<div class="info-box-currency">
    <h5>Курси валют</h5>
    <div class="table-holder">
        <table class="table">
            <!--caption Optional table caption.-->
            <thead>
            <tr>
                <th></th>
                <td>купівля</td>
                <td>продаж</td>
            </tr>
            </thead>
            <tbody>
                <?php Yii::$app->formatter->decimalSeparator = '.';?>
                <?php foreach($currencies as $index=>$curs):?>
                    <tr>
                        <th><?=$index;?></th>
                        <td><?=Yii::$app->formatter->asDecimal($curs['bid'], '2');?></td>
                        <td><?=Yii::$app->formatter->asDecimal($curs['ask'], '2');?></td>
                    </tr>
                <?php endforeach;?>
            </tbody>
        </table>
    </div>
</div>
