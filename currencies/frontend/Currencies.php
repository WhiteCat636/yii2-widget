<?php
namespace frontend\widgets;

use Yii;
use yii\bootstrap\Widget;
use yii\httpclient\Client;

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


class Currencies extends Widget{
    
    public $valute =['USD','EUR','RUB'];
    
    public function run(){
        $cache = Yii::$app->cache;
        $data = $cache->get('currencies');
        if($data==false){
            $data = $this->parseCurrencies();
            if(!$data){
                return;
            }
            $cache->set('currencies', $data);
        }else{
            if(date('d-m-Y', strtotime($data['date'])) != date('d-m-Y')){
                $new_data = $this->parseCurrencies();
                if($new_data){
                    $cache->delete('currencies');
                    $cache->set('currencies', $new_data);
                    $data = $new_data;
                }else{
                    return false;
                }
            }
        }
        return $this->render('@frontend/widgets/views/_currencies', ['currencies'=>$data['currencies']]);
    }
    
    private function parseCurrencies(){
        $client = new Client();
        
        $response = $client->createRequest()
            ->setMethod('get')
            ->setUrl('http://resources.finance.ua/ua/public/currency-cash.json')
            ->send();
        if ($response) {
            $body = $response->data;
        }
        foreach ($body['organizations'] as $organization):  
            if($organization['id']=='7oiylpmiow8iy1sma7w'):
                $privat = $organization;
                $currencies =  $privat['currencies'];
                $data['date'] = $body['date'];
                foreach($currencies as $index=>$curs):
                    if(in_array($index, $this->valute)){
                        $data['currencies'][$index]=
                            [
                                'bid'=>$curs['bid'],
                                'ask'=>$curs['ask']
                            ];
                    }
                endforeach;
                return $data;
            endif;
        endforeach;
        return false;
    }
}