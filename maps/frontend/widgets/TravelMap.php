<?php

namespace frontend\widgets;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
use dosamigos\google\maps\LatLng;
use dosamigos\google\maps\MapAsset;
use dosamigos\google\maps\services\DirectionsWayPoint;
use dosamigos\google\maps\services\TravelMode;
use dosamigos\google\maps\overlays\PolylineOptions;
use dosamigos\google\maps\services\DirectionsRenderer;
use dosamigos\google\maps\services\DirectionsService;
use dosamigos\google\maps\overlays\InfoWindow;
use dosamigos\google\maps\overlays\Marker;
use frontend\widgets\GoogleMap;
use dosamigos\google\maps\services\DirectionsRequest;
use dosamigos\google\maps\overlays\Polygon;
use dosamigos\google\maps\layers\BicyclingLayer;
use Yii;
use yii\bootstrap\Html;
use yii\widgets\Pjax;

class TravelMap extends \yii\bootstrap\Widget
{
    public $_from;
    public $to;
    public $width = 560;
    public $height = 536;
    public $title = 'Title company';

    public function run()
    {
        echo $this->renderView();

    }

    private function renderView()
    {
        Pjax::begin();

        echo Html::beginForm(['#'], 'post', ['data-pjax' => '', 'class' => 'form-inline', 'id' => 'form-map']);
        echo Html::input('text', 'string', Yii::$app->request->post('string'),
            ['class' => 'form-control', 'id' => 'path-input']);
        echo Html::hiddenInput('lat', Yii::$app->request->post('lat'), ['class' => 'form-control', 'id' => 'path-lat']);
        echo Html::hiddenInput('lng', Yii::$app->request->post('lng'), ['class' => 'form-control', 'id' => 'path-lng']);
        echo Html::submitButton('В путь',
            ['id' => 'path-button', 'class' => 'btn btn-lg btn-primary', 'name' => 'start-button']);
        echo Html::endForm();

        echo $this->initMap();

        Pjax::end();
    }



    private function initMap()
    {
        if (!$this->from && !$this->to) {
            return false;
        }
        $coord = new LatLng($this->to);
        $map = new GoogleMap([
            'width'  => $this->width,
            'height' => $this->height,
            'center' => $coord,
            'zoom'   => 14,
        ]);

        $directionsService = $this->directService($map->getName());
        // Thats it, append the resulting script to the map
        $map->appendScript($directionsService->getJs());

        // Add marker to the map
        $map->addOverlay($this->addMarker($coord));

        // Lets show the BicyclingLayer :)
        $bikeLayer = new BicyclingLayer(['map' => $map->getName()]);
        // Append its resulting script
        $map->appendScript($bikeLayer->getJs());

        $map->appendScript($this->formInputJs($map->getName()));

        return $map->display();
    }

    /**
     * @param $mapName
     * @return \dosamigos\google\maps\services\DirectionsService
     */
    private function directService($mapName)
    {
        // lets use the directions renderer
        $from = new LatLng($this->from);
        $to = new LatLng($this->to);

        $directionsRequest = new DirectionsRequest([
            'origin'      => $from,
            'destination' => $to,
            'travelMode'  => TravelMode::DRIVING,
        ]);

        // Lets configure the polyline that renders the direction
        $polylineOptions = new PolylineOptions([
            'strokeColor' => '#FFAA00',
            'draggable'   => false,
        ]);

        // Now the renderer
        $directionsRenderer = new DirectionsRenderer([
            'map'             => $mapName,
            'polylineOptions' => $polylineOptions,
        ]);

        // Finally the directions service
        $directionsService = new DirectionsService([
            'directionsRenderer' => $directionsRenderer,
            'directionsRequest'  => $directionsRequest,
        ]);

        return $directionsService;
    }

    /**
     * @param $coord
     * @return \dosamigos\google\maps\overlays\Marker
     */
    private function addMarker($coord)
    {
        // Lets add a marker now
        $marker = new Marker([
            'position'  => $coord,
            'title'     => 'My Home Town',
            'draggable' => true,

        ]);

        // Provide a shared InfoWindow to the marker
        $marker->attachInfoWindow(
            new InfoWindow([
                'content' => '<p>' . $this->title . '</p>',
            ])
        );

        return $marker;
    }

    /**
     * @param $mapName
     * @return string
     */
    private function formInputJs($mapName)
    {
        $js[] = "var form = (document.getElementById('form-map'));
                var input = (document.getElementById('path-input'));
                var path_lat = (document.getElementById('path-lat'));
                var path_lng = (document.getElementById('path-lng'));
                var place,lat,lng";
        $js[] = $mapName . ".controls[google.maps.ControlPosition.TOP_CENTER].push(form);";
        $js[] = "var autocomplete = new google.maps.places.Autocomplete(input);";
        $js[] = "autocomplete.bindTo('bounds', " . $mapName . ");";
        $js[] = "autocomplete.addListener('place_changed', function () {
                    place = autocomplete.getPlace(); //получаем место
                    lat = place.geometry.location.lat();
                    lng = place.geometry.location.lng();
                    path_lat.value = lat;
                    path_lng.value = lng;
                });";

        return implode("\n", $js);
    }

    /**
     * @return mixed
     */
    public function getFrom()
    {
        return $this->_from = [
            'lat' => Yii::$app->request->post('lat'),
            'lng' => Yii::$app->request->post('lng'),
        ];
    }
}