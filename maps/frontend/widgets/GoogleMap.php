<?php
/**
 * Created by PhpStorm.
 * User: vov
 * Date: 11/13/17
 * Time: 10:06 AM
 */

namespace frontend\widgets;

use Yii;
use dosamigos\google\maps\Map;
use frontend\assets\MapAsset;
use yii\web\View;

class GoogleMap extends Map
{
    /**
     * @param int $position
     */
    public function registerClientScript($position = View::POS_END)
    {
        $view = Yii::$app->getView();
        MapAsset::register($view);
        $this->getPlugins()->registerAssetBundles($view);
        $view->registerJs($this->getJs(), $position);
    }
}