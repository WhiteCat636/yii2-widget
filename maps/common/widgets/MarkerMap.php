<?php

namespace common\widgets;
/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
use dosamigos\google\maps\LatLng;
use dosamigos\google\maps\services\DirectionsWayPoint;
use dosamigos\google\maps\services\TravelMode;
use dosamigos\google\maps\overlays\PolylineOptions;
use dosamigos\google\maps\services\DirectionsRenderer;
use dosamigos\google\maps\services\DirectionsService;
use dosamigos\google\maps\overlays\InfoWindow;
use dosamigos\google\maps\overlays\Marker;
use dosamigos\google\maps\Map;
use dosamigos\google\maps\services\DirectionsRequest;
use dosamigos\google\maps\overlays\Polygon;
use dosamigos\google\maps\layers\BicyclingLayer;
use frontend\widgets\GoogleMap;

class MarkerMap extends \yii\bootstrap\Widget{
    /*example
     * $array =  [
        [
            'lat'=>'50',
            'lng'=>'30',
            'title'=>'Title',
            'description'=>'<p>Description</p> <a href="http://url.url">перейти</a>',
        ]
     * ]
    */
    public $array;
    
    
    public function run(){
        
        $coord = new LatLng(['lat' => 50, 'lng' => 30]);
        $map = new GoogleMap([
            'center' => $coord,
            'zoom' => 14,
        ]);

        foreach ($this->array as $el){
            // Lets add a marker now
            $marker = new Marker([
                'position' => new LatLng(['lat'=>$el['lat'],'lng'=>$el['lng']]),
                'title' => $el['title'],
                'draggable' => false

            ]);

            // Provide a shared InfoWindow to the marker
            $marker->attachInfoWindow(
                new InfoWindow([
                    'content' => $el['description']
                ])
            );

            // Add marker to the map
            $map->addOverlay($marker);
        }
        // Lets show the BicyclingLayer :)
        $bikeLayer = new BicyclingLayer(['map' => $map->getName()]);

        // Append its resulting script
        $map->appendScript($bikeLayer->getJs());
        
        return $map->display();
    }
}